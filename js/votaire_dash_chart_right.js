AmCharts.makeChart("dash_chart_right",
{
	"type": "serial",
	"categoryField": "Age",
	"startDuration": 1,
	"creditsPosition": "bottom-right",
	"categoryAxis": {
		"gridPosition": "start",
		"startOnAxis": true,
		"axisColor": "#ABABAB",
		"gridColor": "",
		"tickLength": 0
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "[[title]] of [[category]]:[[value]]",
			"fillAlphas": 0.7,
			"fillColors": "#A2D0AF",
			"gapPeriod": 0,
			"id": "Social Security",
			"lineAlpha": 0,
			"title": "Social Security",
			"valueAxis": "Income",
			"valueField": "Social Security",
			"xAxis": "Age",
			"yAxis": "Income"
		},
		{
			"balloonText": "[[title]] of [[category]]:[[value]]",
			"fillAlphas": 1,
			"fillColors": "#FFD57F",
			"gapPeriod": 0,
			"id": "Pension",
			"lineAlpha": 0,
			"lineThickness": 0,
			"title": "Pension",
			"valueAxis": "Income",
			"valueField": "Pension"
		},
		{
			"fillAlphas": 1,
			"fillColors": "#FC9860",
			"gapPeriod": 0,
			"id": "Asset Withdrawl",
			"legendColor": "#FC9860",
			"lineThickness": 0,
			"title": "Asset Withdrawl",
			"valueAxis": "Income",
			"valueField": "Asset Withdrawl"
		},
		{
			"fillAlphas": 1,
			"fillColors": "#B7B7B7",
			"id": "Other",
			"legendColor": "#B7B7B7",
			"lineThickness": 0,
			"title": "Other",
			"valueField": "Other"
		},
		{
			"fillAlphas": 1,
			"fillColors": "#29ABE2",
			"id": "QLAC",
			"legendAlpha": 1,
			"legendColor": "#29ABE2",
			"lineThickness": 0,
			"title": "QLAC",
			"valueAxis": "Income",
			"valueField": "QLAC"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "Income",
			"maximum": 1500000,
			"stackType": "regular",
			"unit": "k",
			"axisColor": "#ABABAB",
			"gridColor": "",
			"gridCount": 0,
			"gridThickness": 0,
			"labelsEnabled": false,
			"showFirstLabel": false,
			"title": "",
			"titleBold": false
		},
		{
			"id": "Age",
			"axisColor": "",
			"gridColor": "",
			"tickLength": 0,
			"titleBold": false
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"bottom": 1,
		"equalWidths": false,
		"marginTop": 20,
		"position": "absolute",
		"spacing": 3,
		"top": -20,
		"valueWidth": null,
		"verticalGap": 8
	},
	"titles": [],
	"dataProvider": [
		{
			"Social Security": "220000",
			"Pension": "100000",
			"Asset Withdrawl": "200000",
			"Age": "70",
			"Other": "120000",
			"QLAC": "0"
		},
		{
			"Social Security": "320000",
			"Pension": "140000",
			"Asset Withdrawl": "400000",
			"Age": "75",
			"Other": "140000",
			"QLAC": "0"
		},
		{
			"Social Security": "420000",
			"Pension": "180000",
			"Asset Withdrawl": "300000",
			"Age": "80",
			"Other": "150000",
			"QLAC": "0"
		},
		{
			"Social Security": "500000",
			"Pension": "220000",
			"Asset Withdrawl": "150000",
			"Age": "85",
			"Other": "170000",
			"QLAC": "100000"
		},
		{
			"Social Security": "700000",
			"Pension": "200000",
			"Asset Withdrawl": "80000",
			"Age": "Lifetime",
			"Other": "180000",
			"QLAC": "100000"
		}
	]
}
);