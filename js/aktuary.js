$(document).ready(function() {
    $(".textfield_number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: "." (period)
            (e.keyCode == 190) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});


if (document.getElementById("button_error_resolve"))
{
	document.getElementById("button_error_resolve").onclick = 
	function()
	{
		removeClass(document.getElementsByTagName("body")[0],'form_error_modal');
	};
}

function isNumber(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
/**
 *  Getting started
 */

/*
$(document).ready(function()
{
	$("#getstarted_question_birth_month").keyup(function(e)
	{
		if ($(this).val().length == 2)
		{
			$("#getstarted_question_birth_day").focus();
		}
	});
	$("#getstarted_question_birth_day").keyup(function(e)
	{
		if ($(this).val().length == 2)
		{
			$("#getstarted_question_birth_year").focus();
		}
	});
	
});
*/


$(document).ready(function()
{	
	$(document.getElementsByClassName('datefield_month')).keyup(function(e)
	{
		if ($(this).val().length == 2)
		{
			$(document.getElementsByClassName('datefield_day')).focus();
		}
	});
	$(document.getElementsByClassName('datefield_day')).keyup(function(e)
	{
		if ($(this).val().length == 2)
		{
			$(document.getElementsByClassName('datefield_year')).focus();
		}
	});
	
});

$('#button_gettingstarted_step_2_next').click(function()
{
	var month = document.getElementById("getstarted_question_birth_month").value;
	var day = document.getElementById("getstarted_question_birth_day").value;
	var year = document.getElementById("getstarted_question_birth_year").value;
	var date_error = false;
	var valid_date = false;
	if (
	(month == null	|| month == "") ||
	(month == null	|| month == "") ||
	(day == null	|| day == "") ||
	(day == null	|| day == "") ||
	(year == null	|| year == "") ||
	(year == null	|| year == "")
	)
	{
		date_error = 'empty';
	}
	else
	{
		valid_date = true;
	}
	if (	
	(month < 1		|| month > 12) ||
	(day < 1		|| day > 31) ||
	(year < 1900	|| year > 1997)
	)
	{
		date_error = 'invalid';
		valid_date = false;
	}
	else
	{
		valid_date = true;
	}
	var d = new Date();
	var n = d.getFullYear();
	if (n - year < 18)
	{
		date_error = 'age';
		valid_date = false;
	}
	if (valid_date != true)
	{
		document.getElementById('error_title').innerHTML='Error';
		var error_text;
		switch(date_error)
		{
			case 'invalid':
				error_text = 'Please enter a valid date of birth.';
			break;
			case 'age':
				error_text = 'You must be at least 18 years of age to use this product.';
			break;
			default:
				error_text = 'We require your date of birth in order to continue.';
		} 
		document.getElementById('error_text').innerHTML= error_text;		
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_3'),'active');
		addClass(document.getElementById('gettingstarted_step_2'),'past');
		removeClass(document.getElementById('gettingstarted_step_2'),'active');
		
		addClass(document.getElementById('quote_gettingstarted_step_3'),'active');
		removeClass(document.getElementById('quote_gettingstarted_step_2'),'active');
		
		addClass(document.getElementById('step_gettingstarted_step_3'),'active');
		removeClass(document.getElementById('step_gettingstarted_step_2'),'active');	
	}
	
});
/* Step 3 */
$('#button_gettingstarted_step_3_back').click(function()
{
		addClass(document.getElementById('gettingstarted_step_2'),'active');
		removeClass(document.getElementById('gettingstarted_step_3'),'active');
	
		addClass(document.getElementById('quote_gettingstarted_step_2'),'active');
		removeClass(document.getElementById('quote_gettingstarted_step_3'),'active');
		
		addClass(document.getElementById('step_gettingstarted_step_2'),'active');
		removeClass(document.getElementById('step_gettingstarted_step_3'),'active');
});
$('#button_gettingstarted_step_3_next').click(function()
{
	var field = document.forms["votaire_questions"]["getstarted_question_gender"].value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='Gender is a required field.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_4a'),'active');
		addClass(document.getElementById('gettingstarted_step_3'),'past');
		removeClass(document.getElementById('gettingstarted_step_3'),'active');
	
		addClass(document.getElementById('quote_gettingstarted_step_4a'),'active');
		removeClass(document.getElementById('quote_gettingstarted_step_3'),'active');
	
		addClass(document.getElementById('step_gettingstarted_step_4'),'active');
		removeClass(document.getElementById('step_gettingstarted_step_3'),'active');
	}
});
/* Step 4a */
$('#button_gettingstarted_step_4a_back').click(function()
{
	addClass(document.getElementById('gettingstarted_step_3'),'active');
	removeClass(document.getElementById('gettingstarted_step_4a'),'active');

	addClass(document.getElementById('quote_gettingstarted_step_3'),'active');
	removeClass(document.getElementById('quote_gettingstarted_step_4a'),'active');

	addClass(document.getElementById('step_gettingstarted_step_3'),'active');
	removeClass(document.getElementById('step_gettingstarted_step_4'),'active');
});
$('#button_gettingstarted_step_4a_next').click(function()
{
	var field = document.getElementById("getstarted_question_income_working").value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='We need you to enter your current monthly income.<br />If you don\'t have any, enter 0.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_4b'),'active');
		addClass(document.getElementById('gettingstarted_step_4a'),'past');
		removeClass(document.getElementById('gettingstarted_step_4a'),'active');
	
		addClass(document.getElementById('quote_gettingstarted_step_4bcd'),'active');
		removeClass(document.getElementById('quote_gettingstarted_step_4a'),'active');
	}
});
/* Step 4b */
$('#button_gettingstarted_step_4b_back').click(function()
{
	addClass(document.getElementById('gettingstarted_step_4a'),'active');
	removeClass(document.getElementById('gettingstarted_step_4b'),'active');
	
	addClass(document.getElementById('quote_gettingstarted_step_4a'),'active');
	removeClass(document.getElementById('quote_gettingstarted_step_4bcd'),'active');
});
$('#button_gettingstarted_step_4b_next').click(function()
{
	var field = document.getElementById("getstarted_question_income_retirement").value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='We need you to enter your current monthly retirement income.<br />If you don\'t have any, enter 0.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_4c'),'active');
		addClass(document.getElementById('gettingstarted_step_4b'),'past');
		removeClass(document.getElementById('gettingstarted_step_4b'),'active');
	}
});
/* Step 4c */
$('#button_gettingstarted_step_4c_back').click(function()
{
	addClass(document.getElementById('gettingstarted_step_4b'),'active');
	removeClass(document.getElementById('gettingstarted_step_4c'),'active');
});
$('#button_gettingstarted_step_4c_next').click(function()
{
	var field = document.getElementById("getstarted_question_income_socsec").value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='We need you to enter your current monthly Social Security income.<br />If you don\'t have any, enter 0.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_4d'),'active');
		addClass(document.getElementById('gettingstarted_step_4c'),'past');
		removeClass(document.getElementById('gettingstarted_step_4c'),'active');
	}
});
/* Step 4d */
$('#button_gettingstarted_step_4d_back').click(function()
{
	addClass(document.getElementById('gettingstarted_step_4c'),'active');
	removeClass(document.getElementById('gettingstarted_step_4d'),'active');
});
$('#button_gettingstarted_step_4d_next').click(function()
{
	var field = document.getElementById("getstarted_question_income_salary_bonus").value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='We need you to enter any salary and bonus income.<br />If you don\'t have any, enter 0.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_5'),'active');
		addClass(document.getElementById('gettingstarted_step_4d'),'past');
		removeClass(document.getElementById('gettingstarted_step_4d'),'active');
	
		addClass(document.getElementById('quote_gettingstarted_step_5'),'active');
		removeClass(document.getElementById('quote_gettingstarted_step_4bcd'),'active');
	
		addClass(document.getElementById('step_gettingstarted_step_5'),'active');
		removeClass(document.getElementById('step_gettingstarted_step_4'),'active');
	}
});
/* Step 5 */
$('#button_gettingstarted_step_5_back').click(function()
{
	addClass(document.getElementById('gettingstarted_step_4d'),'active');
	removeClass(document.getElementById('gettingstarted_step_5'),'active');

	addClass(document.getElementById('quote_gettingstarted_step_4bcd'),'active');
	removeClass(document.getElementById('quote_gettingstarted_step_5'),'active');
	
	addClass(document.getElementById('step_gettingstarted_step_4'),'active');
	removeClass(document.getElementById('step_gettingstarted_step_5'),'active');
});
$('#button_gettingstarted_step_5_next').click(function()
{
	var field = document.getElementById("getstarted_question_assets_retirement").value;
	if (field == null || field == "")
	{
		document.getElementById('error_title').innerHTML='Error';
		document.getElementById('error_text').innerHTML='Please enter the estimated value of all retirement assets.';
		addClass(document.getElementsByTagName("body")[0],'form_error_modal');
	    return false;
	}
	else
    {
		addClass(document.getElementById('gettingstarted_step_5'),'active');
		removeClass(document.getElementById('gettingstarted_step_4d'),'active');
	}
});

/**
 *  Dashboard tabs
 */
$('#dash_income_tab_week_button').click(function()
{
	addClass(document.getElementById('dash_income_tab_week'),'active');
	removeClass(document.getElementById('dash_income_tab_month'),'active');
	removeClass(document.getElementById('dash_income_tab_year'),'active');
	
	addClass(document.getElementById('dash_income_content_week'),'active');
	removeClass(document.getElementById('dash_income_content_month'),'active');
	removeClass(document.getElementById('dash_income_content_year'),'active');
	
	return false;
});
$('#dash_income_tab_month').click(function()
{
	addClass(document.getElementById('dash_income_tab_month'),'active');
	removeClass(document.getElementById('dash_income_tab_week'),'active');
	removeClass(document.getElementById('dash_income_tab_year'),'active');
	
	addClass(document.getElementById('dash_income_content_month'),'active');
	removeClass(document.getElementById('dash_income_content_week'),'active');
	removeClass(document.getElementById('dash_income_content_year'),'active');
	return false;
});
$('#dash_income_tab_year').click(function()
{
	addClass(document.getElementById('dash_income_tab_year'),'active');
	removeClass(document.getElementById('dash_income_tab_week'),'active');
	removeClass(document.getElementById('dash_income_tab_month'),'active');
	
	addClass(document.getElementById('dash_income_content_year'),'active');
	removeClass(document.getElementById('dash_income_content_week'),'active');
	removeClass(document.getElementById('dash_income_content_month'),'active');
	return false;
});

/* Dashboard chart hovering tool tips */


if (document.getElementById('chart_hover_retirement_tip') || document.getElementById('chart_hover_lifetime_tip'))
{
	var chart_hover_retirement_tip = document.getElementById('chart_hover_retirement_tip');
	var chart_hover_lifetime_tip = document.getElementById('chart_hover_lifetime_tip');
	window.onmousemove = function (e) {
	    var x = e.clientX,
	        y = e.clientY;
		chart_hover_retirement_tip.style.top = (y + 20) + 'px';
		chart_hover_retirement_tip.style.left = (x + 20) + 'px';
		chart_hover_lifetime_tip.style.top = (y + 20) + 'px';
		chart_hover_lifetime_tip.style.left = (x + 20) + 'px';
	};
}

$('#whatif_scenarios_toggle').click(function()
{
/*
	if (hasClass(document.getElementById('whatif_scenarios'),'finance'))
	{
		removeClass(document.getElementById('whatif_scenarios'),'finance');
	}
	else
	{
		addClass(document.getElementById('whatif_scenarios'),'finance');
	}
	
*/	
	if (hasClass(document.getElementById('whatif_scenarios_finance_term'),'active'))
	{
		removeClass(document.getElementById('whatif_scenarios_finance_term'),'active');
	}
	else
	{
		addClass(document.getElementById('whatif_scenarios_finance_term'),'active');
	}
	
	if (hasClass(document.getElementById('whatif_scenarios_finance'),'active'))
	{
		removeClass(document.getElementById('whatif_scenarios_finance'),'active');
	}
	else
	{
		addClass(document.getElementById('whatif_scenarios_finance'),'active');
	}
});
/*
$(".scroll_top").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});
*/
$(".scroll_top").click(function() {
    $('html, body').animate({
        scrollTop: $("#top").offset().top
    }, 750);
});