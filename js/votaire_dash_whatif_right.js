AmCharts.makeChart("whatif_chart_right",
				{
					"type": "serial",
					"categoryField": "Age",
					"startDuration": 1,
					"creditsPosition": "bottom-right",
					"theme": "light",
					"usePrefixes": true,
					"categoryAxis": {
						"gridPosition": "start",
						"startOnAxis": true,
						"axisColor": "#ABABAB",
						"gridColor": "",
						"tickLength": 0
					},
					"chartCursor": {
						"bulletsEnabled": true,
						"bulletSize": 0,
						"categoryBalloonText": "",
						"cursorColor": "#737373",
						"showNextAvailable": true
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonColor": "#969191",
							"balloonText": "<div style=\"text-align: left !important;\"> <p style='text-align: left;'><div style=\"display:inline-block; width: 20px; height:20px; background-color:#A2D0AF; margin: 0 10px 0 5px; vertical-align:bottom;\"></div>Social Security: <span style='font-size:14px; color:#000000;'>[[Social Security]]</span></p>  <p style='text-align: left;'><div style=\"display:inline-block; width: 20px; height:20px; background-color:#FFD57F; margin: 0 10px 0 5px;\"></div>Pension: <span style='font-size:14px; color:#000000;'>[[Pension]]</span></p>  <p style='text-align: left;'><div style=\"display:inline-block; width: 20px; height:20px; background-color:#FC9860; margin: 0 10px 0 5px; vertical-align:bottom;\"></div>Asset Withdrawl: <span style='font-size:14px; color:#000000;'>[[Asset Withdrawl]]</span></p>  <p style='text-align: left;'><div style=\"display:inline-block; width: 20px; height:20px; background-color:#B7B7B7; margin: 0 10px 0 5px; vertical-align:bottom;\"></div>Other income: <span style='font-size:14px; color:#000000;'>[[Other]]</span></p> </div>",
							"bulletAlpha": 0.54,
							"bulletBorderThickness": 0,
							"fillAlphas": 0.7,
							"fillColors": "#A2D0AF",
							"gapPeriod": 0,
							"id": "Social Security",
							"lineAlpha": 0,
							"lineThickness": 0,
							"title": "Social Security",
							"valueAxis": "Income",
							"valueField": "Social Security",
							"xAxis": "Age",
							"yAxis": "Income"
						},
						{
							"balloonText": "[[title]] at age [[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#FFD57F",
							"gapPeriod": 0,
							"id": "Pension",
							"lineAlpha": 0,
							"lineThickness": 0,
							"showBalloon": false,
							"title": "Pension",
							"valueAxis": "Income",
							"valueField": "Pension"
						},
						{
							"balloonText": "[[title]] at age [[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#FC9860",
							"gapPeriod": 0,
							"id": "Asset Withdrawl",
							"legendColor": "#FC9860",
							"lineThickness": 0,
							"showBalloon": false,
							"title": "Asset Withdrawl",
							"valueAxis": "Income",
							"valueField": "Asset Withdrawl"
						},
						{
							"balloonText": "[[title]] at age [[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#B7B7B7",
							"id": "Other income",
							"lineThickness": 0,
							"showBalloon": false,
							"title": "Other Income",
							"valueField": "Other"
						},
						{
							"fillAlphas": 1,
							"fillColors": "#29ABE2",
							"id": "QLAC",
							"lineThickness": 0,
							"showBalloon": false,
							"title": "QLAC",
							"valueAxis": "Income",
							"valueField": "QLAC"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"baseValue": 26,
							"id": "Income",
							"maximum": 150000,
							"stackType": "regular",
							"totalText": "[[total]]",
							"totalTextColor": "#909090",
							"totalTextOffset": 1,
							"unit": "$",
							"unitPosition": "left",
							"axisColor": "#ABABAB",
							"gridColor": "",
							"gridCount": 0,
							"gridThickness": 0,
							"showFirstLabel": false,
							"title": "",
							"titleBold": false
						},
						{
							"id": "Age",
							"axisColor": "",
							"gridColor": "",
							"showLastLabel": false,
							"tickLength": 0,
							"titleBold": false
						}
					],
					"allLabels": [],
					"balloon": {
						"animationDuration": 0.58,
						"disableMouseEvents": false,
						"showBullet": true
					},
					"titles": [],
					"dataProvider": [
						{
							"Social Security": "14000",
							"Pension": "500",
							"Asset Withdrawl": "5",
							"Age": "65",
							"Other": "35000",
							"QLAC": "0"
						},
						{
							"Social Security": "14000",
							"Pension": "20000",
							"Asset Withdrawl": "30000",
							"Age": "66",
							"Other": "38000",
							"QLAC": "0"
						},
						{
							"Social Security": "14000",
							"Pension": "20000",
							"Asset Withdrawl": "30000",
							"Age": "67",
							"Other": "40000",
							"QLAC": "0"
						},
						{
							"Social Security": "14000",
							"Pension": "20000",
							"Asset Withdrawl": "32000",
							"Age": "68",
							"Other": "41000",
							"QLAC": "0"
						},
						{
							"Social Security": "14000",
							"Pension": "20000",
							"Asset Withdrawl": "35000",
							"Age": "69",
							"Other": "48000",
							"QLAC": "0"
						},
						{
							"Social Security": "14000",
							"Pension": "20000",
							"Asset Withdrawl": "39000",
							"Age": "70",
							"Other": "55000",
							"QLAC": "0"
						}
					]
				}
			);