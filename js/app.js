;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,targ,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
/**
 *  Define a few global functions
 */
 	/**
	 *  Checks whether a specified element has a particular CSS class
	 */
 	function hasClass(element, cls)
	{
		return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
	}
	/**
	 *  Adds a specified CSS class to a given element.
	 *  Checks to see whether the class exists first.
	 */
	function addClass(element,cls)
	{
		if (!hasClass(element,cls))
		{
			 element.className += " "+cls;
		}
	}
	/**
	 *  Removes a specified CSS class from a given element.
	 *  Checks to see whether the class exists first.
	 */
	function removeClass(element,cls)
	{
		if (hasClass(element,cls))
		{
			var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
			element.className=element.className.replace(reg,' ');
		}
	}

/**
 *  Preset Lightbox config
 */
/*
	lightbox.option({
		'showImageNumberLabel': false,
		'fadeDuration': 350,
		'fitImagesInViewport': false
	})
*/
/**
 *  Scroll animation to anchor
 */
jQuery(document).ready(function ($) {
	$('nav a, .navlink').click( function( e ) {
		e.preventDefault();
		var h = e.currentTarget.href;
		var hash = h.substring( h.indexOf('#')+1, h.length );
		var p = $('a[name='+hash+']').position().top;
		$.scrollTo( p, 1000 );
		removeClass(document.getElementById("menu"), 'active');
		removeClass(document.getElementById("mobile-menu-arrow"), 'active');
	});
});
/**
 *  Scroll-based actions
 */
jQuery(document).ready(function ($) {	
	$(window).scroll(function()
	{
		var elements = [];
		var scrollpoints = [];
		var cssclass = [];
		var switchAction = [];
		/**
		 *  Load page elements that require scroll
		 *  locking into the array along with their
		 *  corresponding scroll lock points
		 */
		var i = 0;
/*
		elements[i] = "mast";
		scrollpoints[i] = 220;
		cssclass[i] = "fixed-small";
		switchAction[i] = "both"
		i++;
*/
		/* Loop through the array -- execute the actual code */
		for (i = 0; i < elements.length; i++)
		{
			var e = document.getElementById(elements[i]);
			if (e)
			{
				if($(this).scrollTop() > scrollpoints[i])
				{
					addClass(e,cssclass[i]);
				}
				else
				{
					if (switchAction[i] == "both")
					{
						removeClass(e,cssclass[i]);
					}
				}
			}
		}
	});
});
/**
 *  Navigation menu for mobile/small screen device
 */
if (document.getElementById("mobile-menu-icon"))
{
	var menu = document.getElementById("menu");
	var icon = document.getElementById("mobile-menu-arrow");
	document.getElementById("mobile-menu-icon").onclick = 
	function()
	{
		if (hasClass(menu, 'active'))
		{
			removeClass(menu, 'active');
		}
		else
		{
			addClass(menu, 'active');
		}
		if (hasClass(icon, 'active'))
		{
			removeClass(icon, 'active');
		}
		else
		{
			addClass(icon, 'active');
		}
	};
	/* Close the menu if the user clicks "escape" */
	$(document).on('keydown',function(e)
	{
		if (e.keyCode === 27)
		{
			removeClass(menu, 'active');
			removeClass(icon, 'active');
		}
	});	
}
/**
 *  Toggle between "day" and "night" modes
 */
if (document.getElementById("toggle_day"))
{
	document.getElementById("toggle_day").onclick = 
		function()
		{
			addClass(document.getElementById("toggle_day"), 'active');
			removeClass(document.getElementById("content"), 'night');
			removeClass(document.getElementById("toggle_night"), 'active');
			removeClass(document.getElementById("toggle_auto"), 'active');
		};
}
if (document.getElementById("toggle_night"))
{
	document.getElementById("toggle_night").onclick = 
	function()
	{
		addClass(document.getElementById("toggle_night"), 'active');
		addClass(document.getElementById("content"), 'night');
		removeClass(document.getElementById("toggle_day"), 'active');
		removeClass(document.getElementById("toggle_auto"), 'active');
	};
}
/**
 *  Options page scrolling
 */
	$('#options-scroll-table').click(function() {
	   $('html, body').animate(
	   {
        scrollTop: $("#table").offset().top
       }, 1000);
	});
	$('#options-scroll-chair').click(function() {
	   $('html, body').animate(
	   {
        scrollTop: $("#chair").offset().top
       }, 1000);
	});