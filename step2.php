<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two show-for-small-only"></div>
<div class="vcenter_parent_flex page_height_wrapper_medium_up">
	<div class="row">
		<div class="column small-12">
			<h3 class="steps">You're at <span class="highlight">step 2 of 5</span> to creating your Retirement Income Strategy Report</h3>
		</div>
	</div>
	<div class="row-bleed wrapper_steps relative">
		<div class="bleed_bg_cover grey_light wrapper_steps_inner_wrapper">
			<div class="row stack-10 steps_2_content steps_content">
				<div class="column small-12 medium-12 large-7 small-centered large-uncentered">
					<div class="step_box step_two">
						<p>I was born on</p>
						<ul class="inline_fields">
							<li><input type="text" class="textfield_number textfield_alt text_underline" id="birth_month" placeholder="MM" size="2" maxlength="2" /></li>
							<li><input type="text" class="textfield_number textfield_alt text_underline" id="birth_day" placeholder="DD" size="2" maxlength="2" /></li>
							<li><input type="text" class="textfield_number textfield_alt text_underline" id="birth_year" placeholder="YYYY" size="4" maxlength="4" /></li>
						</ul>
					</div>
					<div class="spacer show-for-small-only"></div>
					<div class="spacer px show-for-medium-up"></div>
					<div class="row">
						<div class="column small-6 align-right">
							<input type="button" name="steps_back" id="steps_back" class="input button fixed-width" value="Back" />
						</div>
						<div class="column small-6 end">
							<input type="button" name="steps_next" id="steps_next" class="input button fixed-width" value="Next" />
						</div>
					</div>
				</div>
				<div class="column small-12 medium-12 large-5">
					<div class="bubble-wrapper">
						<div class="arrow-up"></div>
						<div class="arrow-left"></div>
						<div class="bubble">
							
							<h4>Why are we asking this?</h4>
							<blockquote>
								<p>Your age will help us determine life expectancy. Though this is just an estimate,it is an important factor in assessing risk and using actuarial science to plan for your future financial needs. In our advanced tool,you will be able to input information about your lifestyle, health and family history to create a more precise retirement strategy.</p>
							</blockquote>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






<?php
	require 'includes/footer.inc';
?>