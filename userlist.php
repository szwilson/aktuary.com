<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li class="parent"><a href="#">Admin</a></li><li class="parent"><a href="#">My Profile</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>

<div class="row">
	<div class="column small-12">
		<div class="function_box manage_users">
			<div class="header_content">
				<div class="row">
					<div class="column small-12 medium-6 ">
						<h3 class="function_header">User Management</h3>
					</div>
					<div class="column small-12 medium-6 ">
						<div class="spacer show-for-small-only"></div>							
						<ul class="header_content_menu">
							<li><input type="text" class="input textfield icon_search textfield-small" placeholder="Search users" /></li>
							<li>
								<div class="dropdown">
									<select id="manage_users_select_users" class="field_dropdown manage_users_select_users">
										<option value="all">All Users</option>
									</select>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<table class="table management responsive">
				<tr>
					<th scope="column" class="sort">ID</th>
					<th scope="column" class="sort">First Name</th>
					<th scope="column" class="sort">Last Name</th>
					<th scope="column" class="sort">Email</th>
					<th scope="column" class="sort">Role</th>
					<th scope="column" class="sort">Status</th>
					<th scope="column" class="nosort">Actions</th>
				</tr>
				<tr>
					<td>101</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>102</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>103</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>104</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>105</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>106</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>
				<tr>
					<td>107</td>
					<td>John</td>
					<td>Doe</td>
					<td>john.doe@example.com</td>
					<td>Participant</td>
					<td>40%</td>
					<td>
						<ul class="action_list notext"><li class="edit"><a href="#"><span>Edit</span></a></li><li class="delete"><a href="#"><span>Delete</span></a></li></ul>
					</td>
				</tr>				
			</table>
		</div>
	</div>
</div>




<?php
	require 'includes/footer.inc';
?>