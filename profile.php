<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li class="parent"><a href="#">Admin</a></li><li class="parent"><a href="#">My Profile</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>
<div class="row function_box modify_user">
	<div class="column small-12 nopadding">
		<div class="header_content">
			<h3 class="function_header">My Profile</h3>
		</div>
	</div>
</div>
<div class="row function_box">
	<div class="column small-12 medium-6">
		<div class="spacer two"></div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1">
				<span class="field_label">First Name</span>
			</div>
			<div class="column small-8 medium-7 end">
				<input type="text" name="user_info_name_first" id="user_info_name_first" class="input textfield" value="John" />
			</div>
		</div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1 end">
				<span class="field_label">Last Name</span>
			</div>
			<div class="column small-8 medium-7 end">
				<input type="text" name="user_info_name_first" id="user_info_name_first" class="input textfield" value="Doe" />
			</div>
		</div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1 end">
				<span class="field_label">Email Address</span>
			</div>
			<div class="column small-8 medium-7 end">
				<input type="email" name="user_info_name_email" id="user_info_name_email" class="input textfield" value="john.doe1966@gmail.com" />
			</div>
		</div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1 end">
				<span class="field_label">Password Update</span>
			</div>
			<div class="column small-8 medium-7 end">
				<input type="button" class="button full-width sm" value="Reset Password" />
			</div>
		</div>
	</div>
	<div class="column small-12 medium-6">
		<div class="spacer two"></div>
		<div class="row">		
			<div class="column small-3">
				<span class="field_label">Profile Photo</span>
			</div>
			<div class="column small-9">
				<div class="profile_photo">
					<img src="img/icon_dashboard_profile.svg" />
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</div>
</div>
<div class="row function_box">
	<div class="spacer two"></div>
	<div class="column small-12 small-centered center end">
		<input type="button" name="user_modify_submit" id="user_modify_submit" class="input button fixed-width" value="Update" />
	</div>		
</div>
<div class="row function_box">
	<div class="spacer"></div>
	<hr />
	<div class="spacer"></div>
</div>
<div class="row function_box">
	<div class="column small-11 small-centered end">
		<div class="row">
			<div class="column small-5">
				<h3>My Data</h3>
				<div class="spacer"></div>
			</div>
			<div class="column small-7 medium-4 medium-offset-3 end">
				<input type="text" class="textfield input searchfield" placeholder="Search Questions" />
			</div>
		</div>
	</div>
</div>
<div class="row function_box">
	<div class="column small-6">
		<i class=""></i>
	</div>
	<div class="column small-6">
		<i class=""></i>
	</div>	
</div>
<div class="row function_box">
	<div class="column small-11 small-centered end">
		<!-- Begin all questions/answers -->
		<!-- Question/answer section -->
		<div class="row">
			<div class="column small-12">
				<div class="row">
					<div class="column show-for-medium-up medium-1">
						<i class="profile_question home"></i>
					</div>
					<div class="column small-5 medium-4">
						<h5>Lifestyle Questions</h5>
					</div>
					<div class="column small-6 end">
						<h5>Answer</h5>
					</div>
				</div>
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
			</div>
		</div>
		<!-- End question/answer section -->
		<div class="spacer px"></div>
		<!-- Question/answer section -->
		<div class="row">
			<div class="column small-12">
				<div class="row">
					<div class="column show-for-medium-up medium-1">
						<i class="profile_question health"></i>
					</div>
					<div class="column small-5 medium-4">
						<h5>Health Questions</h5>
					</div>
					<div class="column small-6 end">
						<h5>Answer</h5>
					</div>
				</div>
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
			</div>
		</div>
		<!-- End question/answer section -->
		<div class="spacer px"></div>
		<!-- Question/answer section -->
		<div class="row">
			<div class="column small-12">
				<div class="row">
					<div class="column show-for-medium-up medium-1">
						<i class="profile_question financial"></i>
					</div>
					<div class="column small-5 medium-4">
						<h5>Financial Questions</h5>
					</div>
					<div class="column small-6 end">
						<h5>Answer</h5>
					</div>
				</div>
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
			</div>
		</div>
		<!-- End question/answer section -->
		<div class="spacer px"></div>
		<!-- Question/answer section -->
		<div class="row">
			<div class="column small-12">
				<div class="row">
					<div class="column show-for-medium-up medium-1">
						<i class="profile_question demographic"></i>
					</div>
					<div class="column small-5 medium-4">
						<h5>Demographic Questions</h5>
					</div>
					<div class="column small-6 end">
						<h5>Answer</h5>
					</div>
				</div>
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
			</div>
		</div>
		<!-- End question/answer section -->
		<div class="spacer px"></div>
		<!-- Question/answer section -->
		<div class="row">
			<div class="column small-12">
				<div class="row">
					<div class="column show-for-medium-up medium-1">
						<i class="profile_question goals"></i>
					</div>
					<div class="column small-5 medium-4">
						<h5>Goals Questions</h5>
					</div>
					<div class="column small-6 end">
						<h5>Answer</h5>
					</div>
				</div>
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
				<!-- Question/answer row -->
				<div class="row question_set">
					<div class="column small-5 medium-4 medium-offset-1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-4 medium-5">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="column small-3 medium-2 align-right">
						<ul class="action_list"><li class="edit"><a href="#"><span>Edit</span></a></li></ul>
					</div>
				</div>
				<div class="row">
					<div class="column small-11 small-offset-1">
						<hr />
					</div>					
				</div>
				<!-- End question/answer row -->
			</div>
		</div>
		<!-- End question/answer section -->
		<div class="spacer px"></div>
	<!-- End all questions/answers -->
	</div>
</div>

<?php
	require 'includes/footer.inc';
?>