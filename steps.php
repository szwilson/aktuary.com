<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two show-for-medium-down"></div>
<div class="vcenter_parent_flex page_height_wrapper_medium_up">
	<div class="row">
		<div class="column small-12">
			<h3 class="steps">You're at <span class="highlight">step
				<span id="step_gettingstarted_step_2" class="step_gettingstarted active">2</span>
				<span id="step_gettingstarted_step_3" class="step_gettingstarted">3</span>
				<span id="step_gettingstarted_step_4" class="step_gettingstarted">4</span>
				<span id="step_gettingstarted_step_5" class="step_gettingstarted">5</span>
				 of 5</span> to creating your Retirement Income Strategy Report</h3>
		</div>
	</div>
	<form id="votaire_questions" data-abide>
	<div class="row-bleed wrapper_steps relative">
		<div class="bleed_bg_cover grey_light">
			<div class="wrapper_steps_inner_wrapper">
				<div class="row stack-10 wrapper_steps_flex_wrapper">
					<div class="column small-12 medium-10 medium-centered large-7 small-centered large-uncentered wrapper_steps_inner_wrapper_content">
						<div class="question_content no-margin">
							<div class="spacer show-for-medium-down"></div>
							<!-- Date of birth -->
							<div class="step_box step_two active" id="gettingstarted_step_2">
								<p>I was born on</p>
								<ul class="inline_fields date">
									<li><input type="text" class="textfield_number textfield_alt length-month text_underline datefield_month" id="getstarted_question_birth_month" placeholder="MM" size="2" maxlength="2" required pattern="[1-12]+" /></li>
									<li><input type="text" class="textfield_number textfield_alt length-month text_underline datefield_day" id="getstarted_question_birth_day" placeholder="DD" size="2" maxlength="2" required pattern="[1-12]+" /></li>
									<li><input type="text" class="textfield_number textfield_alt length-year text_underline datefield_year" id="getstarted_question_birth_year" placeholder="YYYY" size="4" maxlength="4" required pattern="[1-12]+" /></li>
								</ul>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-12 center">
										<input type="button" name="steps_next" id="button_gettingstarted_step_2_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>						
							<!-- End date of birth -->
							<!-- Gender -->
							<div class="step_box step_three" id="gettingstarted_step_3">
								<p>I am a</p>
								<div class="question_radio inline">
									<div class="spacer inline half"></div>
									<ul class="inline_fields">
										<li><input type="radio" name="getstarted_question_gender" id="getstarted_question_gender_m"><label for="getstarted_question_gender_m" class="lg" required><span class="icon"></span><span class="text">Male</span></label></li>
										<li><input type="radio" name="getstarted_question_gender" id="getstarted_question_gender_f"><label for="getstarted_question_gender_f" class="lg" required><span class="icon"></span><span class="text">Female</span></label></li>
									</ul>
								</div>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_3_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_3_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- End gender -->
							<!-- Monthly earned (working) income -->
							<div class="step_box step_4a" id="gettingstarted_step_4a">
								<p>My current monthly income<br /> (from salary and wages) is:</p>
								<div class="question_text">
									<ul class="inline_fields">
										<li class="currency"><input type="text" class="textfield_number textfield_alt length-long text_underline currency center" id="getstarted_question_income_working" size="7" maxlength="7" /></li>
									</ul>
								</div>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_4a_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_4a_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- Retirement Income -->
							<div class="step_box step_4b" id="gettingstarted_step_4b">
								<p>My current monthly retirement income is:</p>
								<div class="question_text">
									<ul class="inline_fields">
										<li class="currency"><input type="text" class="textfield_number textfield_alt length-long text_underline currency center" id="getstarted_question_income_retirement" size="7" maxlength="7" /></li>
									</ul>
								</div>
								<p class="small">Include income from pensions, part time jobs, consulting, or annuities. Do not include income from Social Security or retirement investment withdraws.</p>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_4b_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_4b_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- End retirement Income -->
							<!-- Social Security -->
							<div class="step_box step_4c" id="gettingstarted_step_4c">
								<p>My monthly Social Security benefit is:</p>
								<div class="question_text">
									<ul class="inline_fields">
										<li class="currency"><input type="text" class="textfield_number textfield_alt length-medium text_underline currency center" id="getstarted_question_income_socsec" size="7" maxlength="7" /></li>
									</ul>
								</div>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_4c_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_4c_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- End Social Security -->
							<!-- Final working compensation -->
							<div class="step_box step_4d" id="gettingstarted_step_4d">
								<p>My total income from salary and bonus in my final year of employment was:</p>
								<div class="question_text">
									<ul class="inline_fields">
										<li class="currency"><input type="text" class="textfield_number textfield_alt length-long text_underline currency center" id="getstarted_question_income_salary_bonus" size="9" maxlength="9" /></li>
									</ul>
								</div>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_4d_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_4d_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- End final working compensation -->
							<!-- Final working compensation -->
							<div class="step_box step_5" id="gettingstarted_step_5">
								<p>The estimated value of my retirement assets is:</p>
								<div class="question_text">
									<ul class="inline_fields">
										<li class="currency"><input type="text" class="textfield_number textfield_alt length-long text_underline currency center" id="getstarted_question_assets_retirement" size="8" maxlength="9" /></li>
									</ul>
								</div>
								<p class="small">You should include 401k (and similar) accounts, IRAs, stocks, bonds, cash and savings, but not the value of your primary residence or pension.</p>
								<div class="spacer show-for-small-only"></div>
								<div class="spacer px show-for-medium-up"></div>
								<div class="row">
									<div class="column small-6 align-right">
										<input type="button" name="steps_back" id="button_gettingstarted_step_5_back" class="input button fixed-width" value="Back" />
									</div>
									<div class="column small-6 align-left end">
										<input type="button" name="steps_next" id="button_gettingstarted_step_5_next" class="input button fixed-width" value="Next" />
									</div>
								</div>
							</div>
							<!-- End final working compensation -->
						</div>
					</div>
					<div class="column small-12 medium-12 large-5 wrapper_steps_inner_wrapper_content">
						<div class="bubble-wrapper">
							<div class="arrow-up"></div>
							<div class="arrow-left"></div>
							<div class="bubble">
								<h4>Why are we asking this?</h4>
								<blockquote>
									<div id="quote_gettingstarted_step_2" class="quote_gettingstarted active">
										<!-- <p>Your age will help us determine life expectancy. Though this is just an estimate,it is an important factor in assessing risk and using actuarial science to plan for your future financial needs. In our advanced tool,you will be able to input information about your lifestyle, health and family history to create a more precise retirement strategy.</p> -->
									</div>
									<div id="quote_gettingstarted_step_3" class="quote_gettingstarted">
										<p>Your gender is important too. For example, women tend to live longer than men and healthcare costs for males tend to be higher later in life than for females.</p>
									</div>
									<div id="quote_gettingstarted_step_4a" class="quote_gettingstarted">
										<p>Your monthly income from salary (and any other income-generating investments) can be used to estimate the general lifestyle you may expect to live in retirement.</p>
									</div>
									<div id="quote_gettingstarted_step_4bcd" class="quote_gettingstarted">
										<p>Your current retirement income will tell us a little bit about your current strategy, and what we may be able to do to help you spend more now while also reducing the chances of outliving your money.</p>
									</div>
									<div id="quote_gettingstarted_step_5" class="quote_gettingstarted">
										<p class="block">We’ll use the number you provide to estimate, based on capital market assumptions, a rate of return and how quickly you can draw on those investments.</p>
										<p class="block">The types of investments you have, their taxable status, and growth potential can impact the longR term value of your assets.</p>
									</div>
								</blockquote>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<?php
	require 'includes/footer.inc';
?>