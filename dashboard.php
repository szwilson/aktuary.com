<?php
	require 'includes/header.inc';
?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#slider" ).slider();
  });
  </script>
	<script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
	<script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="./js/votaire_dash_chart_left.js"></script>
	<script src="./js/votaire_dash_chart_right.js"></script>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li><a href="#">My Dashboard</a></li><li><a href="#">My Account</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>
<div class="row function_box">
	<div class="column small-12 medium-8 large-6 medium-centered end">
		<div class="dash_section_welcome">
			<div class="welcome_profile_pic">
				<img src="img/icon_dashboard_profile.svg" class="img_profile" />
			</div>
			<div class="welcome_copy">
				<h2>Welcome, John!</h2>
				<div class="welcome_body">
					<p>Your retirement strategy is 45% complete.<br />Answer more questions to improve your results.</p>
				</div>
			</div>
			<div class="welcome_button">
				<input type="button" name="login_submit" id="login_submit" class="input button medium-width" value="Complete Your Profile" />
			</div>			
		</div>
	</div>
</div>
<div class="row function_box grey hide">
	<div class="arrow-down"></div>
	<div class="column small-12 center">
		<div class="spacer px"></div>
		<h2>Optimize My Retirement Strategy</h2>
		<p class="lg">Answer more questions to help us customize our algorithms.</p>
		<div class="spacer"></div>
		<ul class="dash_optimize">
			<li class="demographic"><i></i><div class="alert">4</div><div class="dash_optimize_label">Demographic</div></li>
			<li class="health"><i></i><div class="alert">2</div><div class="dash_optimize_label">Health</div></li>
			<li class="financial"><i></i><div class="alert success"></div><div class="dash_optimize_label">Financial</div></li>
			<li class="lifestyle"><i></i><div class="alert success"></div><div class="dash_optimize_label">Lifestyle</div></li>
			<li class="goals"><i></i><div class="alert success"></div><div class="dash_optimize_label">Goals</div></li>
		</ul>
		<div class="spacer px"></div>
	</div>
</div>
<div class="spacer"></div>
<div class="row function_box">
	<div class="column small-12 medium-12">
		<div class="spacer half"></div>
		<div class="spacer"></div>
		<div class="row">
			<div class="column small-12 center">
				<div class="spacer"></div>
				<h2>Projected Lifetime Retirement Income</h2>
				<div class="spacer"></div>
				<div class="row collapse">
					<div class="column collapse small-12 medium-4 dash_chart_wrapper dash_chart_retirement">
						<div id="dash_chart_left" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
						<div class="chart_hover retirement" id="chart_hover_retirement_tip">
							<h5>Age 66</h5>
							<div class="center">
								<p class="sm center">Annual Income</p>
								<p class="sm income currency center">10,000</p>
							</div>
							<table class="hover_legend">
								<tr>
									<td><i class="legend_ss"></i>Social Security</td>
									<td><span class="currency" id="dash_hover_legend_socsec">14,000</span></td>
								</tr>
								<tr>
									<td><i class="legend_pension"></i>Pension</td>
									<td><span class="currency" id="dash_hover_legend_pension">12,000</span></td>
								</tr>
								<tr>
									<td><i class="legend_asset"></i>Asset Withdrawl</td>
									<td><span class="currency" id="dash_hover_legend_assets">15,000</span></td>
								</tr>
								<tr>
									<td><i class="legend_other"></i>Other Income</td>
									<td><span class="currency" id="dash_hover_legend_other">6,500</span></td>
								</tr>
							</table>
							<div class="arrow-left"></div>
						</div>
					</div>
					<div class="column collapse small-12 medium-8 dash_chart_wrapper dash_chart_lifetime">
						<div id="dash_chart_right" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
						<div class="chart_hover lifetime" id="chart_hover_lifetime_tip">
							<h5>Looking Ahead</h5>
							<p>Your retirement income strategy after 5 years is a projection, based on your current information and market conditions. This graph represents your personalized actuarial model.</p>
							<div class="arrow-left"></div>
						</div>						
					</div>					
				</div>
			</div>
		</div>
		<div class="spacer two"></div>
		<div class="row">
			<div class="column small-10 small-centered">			
				<div class="row">
					<div class="column small-10 small-centered medium-5 medium-uncentered">
						<h5 class="inline">Retirement Age</h5>
						<span id="retirementAge" class="dash_slider_label inline"></span>
						<div class="range-slider" data-slider data-options="start: 55; end: 75; display_selector: #retirementAge;">
							<span class="range-slider-handle" role="slider" tabindex="0"></span>
							<span class="range-slider-active-segment"></span>
						</div>
					</div>
					<div class="column small-10 small-centered medium-5 medium-offset-2 medium-uncentered">
						<h5>Market Performance</h5>
						<div class="range-slider" data-slider data-options="start: 1; end: 3; step: 1; display_selector: #marketPerformance;">
							<span class="range-slider-handle" role="slider" tabindex="0"></span>
							<span class="range-slider-active-segment"></span>
							<!-- <span id="marketPerformance" class="dash_slider_label"></span> -->
						</div>
						<ul class="slider_input_label">
							<li>Poor</li>
							<li>Average</li>
							<li>Excellent</li>
						</ul>
					</div>				
				</div>
			</div>
		</div>
		<div class="spacer two"></div>
		<div class="row">
			<div class="column small-10 small-centered">			
				<div class="row">
					<div class="column small-10 small-centered medium-5 medium-uncentered">
						<h5 class="inline">Start Social Security Age</h5>
						<span id="beginSocSec" class="dash_slider_label inline"></span>
						<div class="range-slider" data-slider data-options="start: 59; end: 70; display_selector: #beginSocSec;">
							<span class="range-slider-handle" role="slider" tabindex="0"></span>
							<span class="range-slider-active-segment"></span>
						</div>
					</div>
					<div class="column small-10 small-centered medium-5 medium-offset-2 medium-uncentered center">
						<div class="spacer two show-for-medium-up"></div>
						<input type="button" name="login_submit" id="login_submit" class="input button medium-width" value="Re-Calculate" />
					</div>				
				</div>
			</div>
		</div>
		<div class="spacer px"></div>
		<div class="row">
			<div class="column small-10 small-centered end">
				<h2 class="center">What if I invest in a QLAC?</h2>
			</div>
		</div>
		<div class="row">
			<div class="column small-10 small-centered medium-7 end">
				<p>A Qualified Lifetime Annuity Contract (“QLAC”) is a retirement tool that allows you to purchase a lump-sum annuity after the age of 55. The QLAC is a differed annuity, starting a payout later in life as a means to assure additional lifetime income in your later years. QLACs do not pay anything to your estate in the event you die prior to the qualifying age.</p>
				<div class="center">
					<p>Include QLAC?</p>
					<div class="switch round small">
						<input id="include_qlac_check" type="checkbox" name="include_qlac">
						<label for="include_qlac_check"></label>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<div class="spacer"></div>
	<div class="row function_box">
		<div class="column small-12">
			<div class="row">
				<div class="column small-12">
					<div class="spacer two"></div>
					<h2 class="center">What Can I Afford to Do?</h2>
					<div class="spacer"></div>
				</div>
			</div>
			<div class="spacer"></div>
			<div class="row">
				<div class="column small-12 medium-4 medium-offset-1">
					<h4 class="sm">What if I make a major purchase?</h4>
					<p>Are you considering making a major purchase such as a vacation property, a new car, or taking a vacation? Use our quick calculator to determine how this purchase will impact your future retirement income.</p>
					<div class="spacer px show-for-medium-up"></div>
				</div>
				<div class="column small-12 medium-6 medium-offset-1">
					<div class="spacer"></div>
					<ul class="dash_optimize assumptions">
						<li class="car"><i></i><div class="dash_optimize_label">Buy a car</div></li>
						<li class="vacation"><i></i><div class="dash_optimize_label">Plan a vacation</div></li>
						<li class="home"><i></i><div class="dash_optimize_label">Purchase a home</div></li>
					</ul>
					<div class="spacer px"></div>
					<div class="center">
						<a href="/aktuary-app/whatif" class="button medium-width">Start calculating</a>

					</div>
					<div class="spacer px show-for-medium-up"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="spacer px"></div>
<div class="row">
	<div class="column small-7 small-centered end">
		<h4 class="center">Assumptions</h4>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ex eros, cursus eget magna et, consequat tincidunt tortor. Ut maximus placerat metus, sit amet auctor orci aliquet id. Vestibulum neque felis, dapibus a ligula et, viverra pulvinar purus. More</p>
	</div>
</div>
<div class="spacer px"></div>
<?php
	require 'includes/footer.inc';
?>