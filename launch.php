<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="row-bleed hero_launch vcenter_parent_flex">
	<div class="row center_vertical relative">
		<div class="column small-12 medium-7 center medium-push-5 code_wrapper_outer large-6 large-offset-1">
			<div class="code_wrapper">
				<div class="spacer show-for-small-only"></div>
				<h2 class="alt">Already have a code?</h2>
				<div class="spacer half"></div>
				<div class="code_wrapper_inner">
					<ul class="passcode">
						<li><input type="text" class="textfield textfield_number passcode_field" name="passcode_field_0" id="passcode_field_0" size="1" maxlength="1" /></li><li><input type="text" class="textfield textfield_number passcode_field" name="passcode_field_1" id="passcode_field_1" size="1" maxlength="1" /></li><li><input type="text" class="textfield textfield_number passcode_field" name="passcode_field_2" id="passcode_field_2" size="1" maxlength="1" /></li><li><input type="text" class="textfield textfield_number passcode_field" name="passcode_field_3" id="passcode_field_3" size="1" maxlength="1" /></li><li><input type="text" class="textfield textfield_number passcode_field" name="passcode_field_4" id="passcode_field_4" size="1" maxlength="1" /></li></ul>					
				</div>
					<div class="spacer"></div>
					<input type="button" name="login_submit" id="login_submit" class="input button medium-width" value="Submit" />
			</div>
		</div>
		<div class="column small-12 medium-5 medium-pull-7 medium-offset-1">
			<div class="spacer show-for-small-only"></div>
			<div class="letter">
				<p>A new tool to enable you to manage your retirement income strategy is coming soon. We’re still in beta mode&mdash;so only those with an authorization code can access our service at this time.</p>
				<p>Don’t have a code? Ask to receive an invitation below.</p>
				<p>&ndash; The Aktuary Team</p>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="spacer px show-for-medium-up"></div>
	<div class="column small-12 small-centered medium-8 end large-6">
		<h3>Don't have an invitation code yet?</h3>
		<p>Sign up below to request an invitation code. We are not adding new users at this time. But, if selected to participate in the next round of our beta launch, we’ll invite you to join us!</p>		
	</div>
</div>
<div class="row">
	<div class="column small-6 medium-4 medium-offset-2 large-3 large-offset-3">
		<input type="text" name="launch_request_name_first" id="launch_request_name_first" class="input textfield" placeholder="First Name" />
	</div>
	<div class="column small-6 medium-4 end large-3">
		<input type="text" name="launch_request_name_last" id="launch_request_name_last" class="input textfield" placeholder="Last Name" />
	</div>	
</div>
<div class="row">
	<div class="column small-12 medium-8 medium-centered large-6 end">
		<input type="text" name="launch_request_email" id="launch_request_email" class="input textfield" placeholder="Email Address" />
	</div>
</div>
<div class="row">
	<div class="column small-12 medium-4 medium-centered large-3 end">
		<input type="button" name="launch_request_submit" id="launch_request_submit" class="input button full-width" value="Request an invite" />
	</div>
</div>
<div class="spacer two px"></div>





<?php
	require 'includes/footer.inc';
?>