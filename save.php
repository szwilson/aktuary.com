<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two show-for-small-only"></div>
<div class="vcenter_parent_flex page_height_wrapper_medium_up">
		<div class="row-bleed wrapper_steps relative">
			<div class="bleed_bg_cover grey_light relative wrapper_steps_inner_wrapper">
				<div class="wrapper_steps_inner_wrapper_content center_block screen_save">
					<div class="row stack-10 steps_save_content steps_content">
						<div class="column small-12 medium-9 medium-centered large-7 large-push-5 large-uncentered">				
							<h3 class="alt">Your retirement withdraw<br />strategy is almost ready</h3>
							<p>Our actuaries are crunching your financial results, let us know where you would like us to send your report.</p>
							<div class="row">
								<div class="column small-6">
									<input type="text" name="save_name_first" id="save_name_first" class="input textfield" placeholder="First Name" />
								</div>
								<div class="column small-6">
									<input type="text" name="save_name_last" id="save_name_last" class="input textfield" placeholder="Last Name" />
								</div>
							</div>
							<div class="row">
								<div class="column small-12">
									<input type="email" name="save_email" id="save_email" class="input textfield" placeholder="Email Address" />
								</div>
							</div>
							<div class="row">
								<div class="column small-12 center">
									<div class="spacer"></div>
									<input type="button" name="save_submit" id="save_submit" class="input button medium-width" value="Calculate my income" />
								</div>
							</div>
						</div>
					
						
						
						<div class="column large-5 small-centered large-uncentered large-pull-7 show-for-large-up">
							<div class="content_block">
								<div class="center">
									<img src="img/steps_save_graphic.jpg" class="dash_save_graphic" />
								</div>
							</div>
						</div>

						
						
						
					
					
					
					</div>
				</div>
			</div>
		</div>
</div>
<?php
	require 'includes/footer.inc';
?>