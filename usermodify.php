<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li class="parent"><a href="#">Admin</a></li><li class="parent"><a href="#">My Profile</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>

<div class="row function_box modify_user">
	<div class="column small-12 nopadding">
		<div class="header_content">
			<h3 class="function_header">Modify User</h3>
			<div class="menu">
				<div class="text">
					<i class="fa fa-caret-left"></i> <a href="#">Back to Users</a>
				</div>
			</div>
		</div>
	</div>
</div>
	
<div class="row function_box">
	<div class="column small-12 medium-6">
		<div class="spacer two"></div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1">
				<span class="field_label">First Name</span>
			</div>
			<div class="column small-8 medium-7 medium-offset-1 last">
				<input type="text" name="user_info_name_first" id="user_info_name_first" class="input textfield" value="John" />
			</div>
		</div>
		<div class="row">		
			<div class="column small-4 medium-3 medium-offset-1">
				<span class="field_label">Last Name</span>
			</div>
			<div class="column small-8 medium-7 medium-offset-1 end">
				<input type="text" name="user_info_name_first" id="user_info_name_first" class="input textfield" value="Doe" />
			</div>
		</div>
	</div>		
	<div class="column small-12 medium-6">
		<div class="function_box">
			<div class="spacer two"></div>
			<div class="row">		
				<div class="column small-4 medium-4">
					<span class="field_label">Email Address</span>
				</div>
				<div class="column small-8 medium-7 end">
					<input type="text" name="user_info_name_first" id="user_info_name_first" class="input textfield" value="john.doe1966@gmail.com" />
				</div>
			</div>
			<div class="row">		
				<div class="column small-4 medium-4">
					<span class="field_label">User Role</span>
				</div>
				<div class="column small-8 medium-7 end">
					<select id="modify_user_select_role" class="modify_user_select_role">
						<option value="participant">Participant</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row function_box">
	<div class="spacer two"></div>
	<div class="column small-6 align-right">
		<input type="button" name="user_password_reset" id="user_password_reset" class="input button fixed-width alt_grey" value="Reset Password" />
	</div>
	<div class="column small-6 end">
		<input type="button" name="user_modify_submit" id="user_modify_submit" class="input button fixed-width" value="Submit" />
	</div>		
</div>
<div class="row function_box">
	<div class="spacer"></div>
	<hr />
	<div class="spacer"></div>
</div>
<div class="row function_box">
	<div class="column small-12 medium-6">
		<div class="row">
			<div class="column small-12 medium-11 medium-offset-1">
				<h3>User Detail</h3>
			</div>
		</div>
	</div>
</div>
	<div class="row function_box">
		<div class="column small-12 medium-6">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Question Category</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row function_box">
		<div class="column small-12 medium-6">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Question</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
				</div>
			</div>
		</div>
		<div class="column small-12 medium-5 end">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Answer</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
				</div>
			</div>
			<div class="spacer px"></div>
		</div>
	</div>
	

	<div class="row function_box">
		<div class="column small-12 medium-6">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Question Category</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row function_box">
		<div class="column small-12 medium-6">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Question</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
				</div>
			</div>
		</div>
		<div class="column small-12 medium-5 end">
			<div class="row">
				<div class="column small-12 medium-11 medium-offset-1">
					<h4>Answer</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
				</div>
			</div>
			<div class="spacer px"></div>
		</div>
	</div>





<?php
	require 'includes/footer.inc';
?>