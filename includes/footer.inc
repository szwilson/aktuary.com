<footer class="row-bleed">
	<div class="row">
		<div class="column small-12 medium-6">
			<div class="copyright">&copy; 2015 Votaire, LLC. All rights reserved.</div>
		</div>
		<div class="column small-12 medium-6 relative">
			<ul class="footer_menu">
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms of Use</a></li>
			</ul>
		</div>
	</div>
</footer>
		
		<script src="./js/app.js"></script>
		<script src="./js/aktuary.js"></script>
		<script src="./js/foundation.min.js"></script>
		<script src="./js/responsive-tables.js"></script>
		<script>
		  $(document).foundation();
		</script>
	</body>
</html>