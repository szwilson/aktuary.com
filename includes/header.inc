<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7;" />
		<title>Votaire</title>
		<script src="./js/vendor/modernizr.js"></script>
		<script src="//use.typekit.net/dgi6xux.js"></script>
		<script src="//code.jquery.com/jquery-latest.min.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="./css/normalize.css">
		<link rel="stylesheet" type="text/css" href="./css/foundation.min.css" />
		<link rel="stylesheet" type="text/css" href="./css/responsive-tables.css" />
		<link rel="stylesheet" type="text/css" href="./css/app.css">
		<link rel="stylesheet" type="text/css" href="./css/addons.css">
		<link rel="stylesheet" type="text/css" href="./css/aktuary.css">
	</head>
	<body class="">
	<div id="flow_error" class="vcenter_parent_flex">
		<div class="row error_row">
			<div class="column small-12">
				<h2 id="error_title">Error</h2>
				<p id="error_text">An error has occurred.</p>
				<div class="spacer two"></div>
				<input type="button" class="button medium-width" id="button_error_resolve" value="OK" />
			</div>
		</div>
	</div>