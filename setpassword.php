<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-12">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="row login_box_wrapper">
	<div class="column small-12 medium-10 medium-centered login_box_inner_wrapper">
		<div class="function_box login_box">
			<div class="header_content">
				<h3 class="function_header">Set new password</h3>
			</div>
			<div class="login_box_content">
				<div class="row">
					<div class="column small-10 small-centered medium-4 medium-uncentered login_label">
						<label for="login_password">Password</label>
					</div>
					<div class="column small-10 small-centered medium-5 medium-uncentered end">
						<input type="password" name="login_password" id="login_password" class="input textfield" />
					</div>
				</div>
				<div class="row">
					<div class="column small-10 small-centered medium-4 medium-uncentered login_label">
						<label for="login_password">Confirm Password</label>
					</div>
					<div class="column small-10 small-centered medium-5 medium-uncentered end">
						<input type="password" name="login_password_confirm" id="login_password_confirm" class="input textfield" />
					</div>
				</div>
				
				<div class="row">
					<div class="column small-10 small-centered medium-5 medium-uncentered medium-offset-4 end center">
						<input type="button" name="login_submit" id="login_submit" class="input button full-width" value="Set Password" />
						<p class="align-left">Passwords must be a minimum of eight characters, contain at least one number, and at least one symbol.</p>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<?php
	require 'includes/footer.inc';
?>