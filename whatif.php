<?php
	require 'includes/header.inc';
?>
	<script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
	<script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="./js/votaire_dash_whatif_left.js"></script>
	<script src="./js/votaire_dash_whatif_right.js"></script>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li class="parent"><a href="#">Admin</a></li><li class="parent"><a href="#">My Profile</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>
<div class="row">
	<div class="column small-12 center">
		<h2>What Can I Afford to Do?</h2>
	</div>
</div>
<a name="top" id="top"></a>
<div class="spacer two"></div>
<div class="row function_box">
	<div class="column small-12">
		<div class="row" id="whatif_scenarios">
			<div class="spacer two"></div>
			<div id="whatif_scenarios_info" class="column small-10 small-centered medium-3 medium-uncentered center">
				<h3>What's the impact?</h3>
				<p>Major purchases outside of your annual retirement income can impact your long-term sustainability of assets.</p>
				<h4 class="sm">I plan to finance this purchase</h4>
				<div class="switch round small">
					<input id="whatif_scenarios_toggle" type="checkbox" name="whatif_toggle">
					<label for="whatif_scenarios_toggle"></label>
					<span class="on">Yes</span>
					<span class="off">No</span>
				</div>
				<div class="spacer px show-for-small-only"></div>
			</div>
			<div id="whatif_scenarios_purchase" class="column small-10 small-centered medium-4 medium-offset-1 medium-uncentered center medium-uncenter">
				<p class="sm">Purchase Price</p>
				<div class="textfield_currency">
					<input type="text" class="textfield currency textfield_number" name="whatif_purchase_price" />
				</div>
				<div class="spacer"></div>
				<div id="whatif_scenarios_finance_term">
					<p class="sm">Payment Term (in years)</p>
					<div class="textfield_currency">
						<input type="text" class="textfield currency textfield_number" name="whatif_payment_terms" />
					</div>
					<div class="spacer two show-for-small-only"></div>
				</div>
			</div>
			<div id="whatif_scenarios_finance" class="column small-10 small-centered medium-4 medium-uncentered center medium-uncenter">
				<p class="sm">Down Payment</p>
				<div class="textfield_currency">
					<input type="text" class="textfield currency textfield_number" name="whatif_payment_down" />
				</div>			
				<div class="spacer"></div>
				<p class="sm">Monthly Payment</p>
				<div class="textfield_currency">
					<input type="text" class="textfield currency textfield_number" name="whatif_payment_monthly" />
				</div>
				<div class="spacer two show-for-small-only"></div>
			</div>
		</div>
		<div class="row">
			<div class="column small-12 center medium-8 medium-offset-4 medium-uncenter">
				<input type="button" class="button medium-width" id="whatif_calculate" value="Run the Numbers" />
				<div class="spacer two show-for-small-only"></div>
			</div>
		</div>
	</div>
</div>
<div class="spacer px"></div>
<div class="row">
	<div class="column small-10 small-centered center">
		<h2>My Annual Income</h2>
	</div>
</div>
<div class="spacer px"></div>
<div class="row collapse">
	<div class="column small-12">
		<div class="whatif_graph_wrapper">
			<div class="center">
				<div class="spacer"></div>
				<h4 class="sm">Before purchase</h4>
				<h2 class="currency gold" id="what_if_graph_left_amount">6,000</h2>
			</div>
			<div id="whatif_chart_left" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
		</div>
		<div class="spacer px show-for-small-only"></div>
		<div class="whatif_graph_wrapper">
			<div class="center">
				<div class="spacer"></div>
				<h4 class="sm">After purchase</h4>
				<h2 class="currency gold" id="what_if_graph_left_amount">3,000</h2>
			</div>
			<div id="whatif_chart_right" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
		</div>
	</div>
</div>
<div class="spacer px"></div>
<div class="row show-for-small-only">
	<div class="column small-12 center">
		<input type="button" class="button medium-width scroll_top" id="whatif_calculate_again" value="Run Numbers Again" />
	</div>
</div>
<div class="spacer px show-for-small-only"></div>
<?php
	require 'includes/footer.inc';
?>