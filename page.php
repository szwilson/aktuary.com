<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4 end">
			<div class="logo">Logo</div>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>
<div class="row function_box content">
	<div class="column small-12">
		
		<h1>This is a Level 1 heading</h1>
		<p>This section contains examples of various styles and how to apply them. These two sentences are standard paragraphs.</p>
		<ol class="noindent">
			<li>This list is intentionally not indented. Add a CSS class <code>noindent</code>.</li>
			<li><strong>This is bold content. Use the <code>&lt;strong&gt;</code> tag.</strong></li>
			<li><em>This is italicized text. Use the <code>&lt;em&gt;</code> tag.</em></li>
		</ol>
		<p>Some more example paragraph text. This has been the first example section.</p>
		<h2>Level 2 heading</h2>
		<p>The following is an example unordered list. By default, both list styles (ordered and unordered) are indented.</p>
		<ul>
			<li>Sed et augue vel eros convallis faucibus.</li>
			<li>Mauris non neque dictum dolor ultrices auctor.</li>
			<li>Suspendisse sed ligula vitae diam mattis malesuada.</li>
			<li>Integer ac dolor at justo facilisis lobortis.</li>
		</ul>
		<h3>Level 3 heading</h3>
		<p>Below is a table example. Use tables only for tabular data. A table will be a fixed width, but will expand/collapse if the width exceeds the content area.
		<table>
			<thead>
				<tr>
					<th>Column Header 1</th>
					<th>Column Header 2</th>
					<th>Column Header 3</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Column row 1 cell 1</td>
					<td>Column row 1 cell 2</td>
					<td>Column row 1 cell 3</td>
				</tr>
				<tr>
					<td>Column row 2 cell 1</td>
					<td>Column row 2 cell 2</td>
					<td>Column row 2 cell 3</td>
				</tr>
			</tbody>
		</table>
		<p>Ut accumsan aliquet odio sed semper. Vestibulum feugiat, eros ac egestas pharetra, quam turpis sodales est, nec scelerisque sem nunc ac erat. Sed sagittis est non tellus placerat, quis ultrices massa fermentum. Aliquam vitae eros eget erat porttitor fringilla nec a eros. Donec hendrerit tincidunt risus nec vehicula. Quisque vel purus convallis, vehicula urna non, congue enim. Aenean eu commodo felis.</p>
		<h4>This is a level 4 heading</h4>
		<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed gravida ipsum est, eget tincidunt nulla venenatis non. Donec dapibus efficitur enim, nec pulvinar ligula interdum nec. Donec non interdum eros. In nec orci enim. Maecenas dignissim magna eu malesuada fermentum. Pellentesque finibus, tortor non condimentum lobortis, augue nisl sagittis purus, eu sodales nisl urna quis lectus. Nulla egestas laoreet porta. Pellentesque dictum dolor sed magna tincidunt scelerisque. Vivamus viverra nulla ac orci aliquam, dapibus euismod nisi tempus.</p>
		<h5>This is a Level 5 heading</h5>
		<p>Level 5 headings are usually unnecessary in body copy&mdash;but they are styled just in case. Also note they are a different color than Headings level 1&ndash;4.</p>
	</div>		
</div>



<?php
	require 'includes/footer.inc';
?>