<?php
	$type = $_GET['type'];
?>
<?php
	require 'includes/header.inc';
?>
<header class="row-bleed">
	<div class="row">
		<div class="column small-4">
			<div class="logo">Logo</div>
		</div>
		<div class="column small-8 align-right">
			<nav>
				<ul class="menu"><li><a href="#">My Dashboard</a></li><li><a href="#">My Account</a></li><li><a href="#">Sign Out</a></li></ul>
			</nav>
		</div>
	</div>
</header>
<div class="header_spacer"></div>
<div class="spacer two"></div>
<div class="row show-for-medium-up">
	<div class="column small-12 small-centered end center">
		<div class="spacer px"></div>
		<h2>Optimize My Retirement Strategy</h2>
		<div class="spacer"></div>
		<ul class="dash_optimize sm" id="dash_optimize_questions">
			<li class="demographic"><i></i><div class="alert">4</div><div class="dash_optimize_label">Demographic</div></li>
			<li class="health"><i></i><div class="alert">2</div><div class="dash_optimize_label">Health</div></li>
			<li class="financial"><i></i><div class="alert success"></div><div class="dash_optimize_label">Financial</div></li>
			<li class="lifestyle"><i></i><div class="alert success"></div><div class="dash_optimize_label">Lifestyle</div></li>
			<li class="goals"><i></i><div class="alert success"></div><div class="dash_optimize_label">Goals</div></li>
		</ul>
		<div class="spacer px"></div>
	</div>
</div>
<div class="spacer"></div>
	<div class="row">
		<div class="column small-11 small-centered small-uncollapse end">
			<div class="row">
				<div class="function_box column small-12 medium-8 vcenter_parent_flex center relative">
					<div class="question_content question_content_wrapper">
					<!-- Radio -->
					<?php
					switch ($type)
					{
					case 'radio':
					?>
						<div class="question_radio">
							<h3>How often do you smoke?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-11 medium-8 small-centered align-left end">						
									<ul>
										<li><input type="radio" name="radio_question_name" id="radio_question_name_answer_id1"><label for="radio_question_name_answer_id1"><span class="icon"></span><span class="text">I have never smoked</span></label></li>
										<li><input type="radio" name="radio_question_name" id="radio_question_name_answer_id2"><label for="radio_question_name_answer_id2"><span class="icon"></span><span class="text">I smoke 2 or more packs per day</span></label></li>
										<li><input type="radio" name="radio_question_name" id="radio_question_name_answer_id3"><label for="radio_question_name_answer_id3"><span class="icon"></span><span class="text">I smoke less than 2 packs per day</span></label></li>
										<li><input type="radio" name="radio_question_name" id="radio_question_name_answer_id4"><label for="radio_question_name_answer_id4"><span class="icon"></span><span class="text">I quit smoking less than 2 years ago</span></label></li>
										<li><input type="radio" name="radio_question_name" id="radio_question_name_answer_id5"><label for="radio_question_name_answer_id5"><span class="icon"></span><span class="text">I quit smoking more than 2 years ago</span></label></li>
									</ul>									
								</div>
							</div>
							<div class="spacer two"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php break; ?>
					<!-- End Radio -->
					<!-- Checkbox -->
					<?php
					case 'checkbox':
					?>
						<div class="question_checkbox">
							<h3>How often do you smoke?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-11 medium-8 small-centered align-left end">						
									<ul>
										<li><input type="checkbox" name="checkbox_question_name" id="checkbox_question_name_answer_id1"><label for="checkbox_question_name_answer_id1"><span class="icon"></span><span class="text">I have never smoked</span></label></li>
										<li><input type="checkbox" name="checkbox_question_name" id="checkbox_question_name_answer_id2"><label for="checkbox_question_name_answer_id2"><span class="icon"></span><span class="text">I smoke 2 or more packs per day</span></label></li>
										<li><input type="checkbox" name="checkbox_question_name" id="checkbox_question_name_answer_id3"><label for="checkbox_question_name_answer_id3"><span class="icon"></span><span class="text">I smoke less than 2 packs per day</span></label></li>
										<li><input type="checkbox" name="checkbox_question_name" id="checkbox_question_name_answer_id4"><label for="checkbox_question_name_answer_id4"><span class="icon"></span><span class="text">I quit smoking less than 2 years ago</span></label></li>
										<li><input type="checkbox" name="checkbox_question_name" id="checkbox_question_name_answer_id5"><label for="checkbox_question_name_answer_id5"><span class="icon"></span><span class="text">I quit smoking more than 2 years ago</span></label></li>
									</ul>									
								</div>
							</div>
							<div class="spacer two"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php break; ?>
					<!-- End checkbox -->
					<!-- freeform text -->
					<?php
					case 'text':
					?>
						<div class="question_text">
							<h3>Can you answer a freeform question?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-11 medium-8 small-centered align-left end">						
									<input type="text" class="textfield" />									
								</div>
							</div>
							<div class="spacer two"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php break; ?>
					<!-- End Freeform Text -->
					<!-- Date -->
					<?php
					case 'date':
					?>
						<div class="question_text">
							<h3>Can you answer a date question?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-12 medium-8 small-centered align-left end">
									<div class="step_box active">
										<ul class="questions_date date">
											<li><input type="text" class="textfield_number length-month text_underline" id="birth_month" placeholder="MM" size="2" maxlength="2" /></li>
											<li><input type="text" class="textfield_number length-month text_underline" id="birth_day" placeholder="DD" size="2" maxlength="2" /></li>
											<li><input type="text" class="textfield_number length-year text_underline" id="birth_year" placeholder="YYYY" size="4" maxlength="4" /></li>
										</ul>
									</div>									
								</div>
							</div>
							<div class="spacer two"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php break; ?>
					<!-- End date -->
					<!-- Slider -->
					<?php
					case 'slider':
					?>
						<div class="question_text">
							<h3>Can you answer a question needing a slider?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-12 medium-8 small-centered align-left end">
									<div class="range-slider" data-slider>
										<span class="range-slider-handle" role="slider" tabindex="0"></span>
										<span class="range-slider-active-segment"></span>
										<input type="hidden">
									</div>
								</div>
							</div>
							<div class="spacer two"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php break; ?>
					<!-- End slider -->
					<!-- Dropdown -->
					<?php
					case 'dropdown':
					default:
					?>
						<div class="question_dropdown">
							<h3>What is your marital status?</h3>
							<div class="spacer"></div>
							<div class="row">
								<div class="column small-10 medium-6 small-centered end">						
									<select>
										<option value="--">Choose one</option>
										<option value="married">Married</option>
										<option value="single">Not Married</option>
									</select>
								</div>
							</div>
							<div class="spacer"></div>
							<input class="button medium-width" type="button" value="Save and Next" />
						</div>
					<?php } ?>
					<!-- End slider -->
						<div class="question_bottom_content">
							<a href="#" id="question_skip_marital" class="question_bottom_link">Skip question</a>
						</div>
					</div>
				</div>
				<div class="column small-12 show-for-small-only">
					<div class="spacer px"></div>
				</div>
				<div class="column small-12 medium-4 end">
					<div class="function_box center">
						<div class="spacer"></div>
						<p class="nomargin">My income this month is:</p>
						<div class="currency dash_total main-sm">878.50</div>
						<div class="spacer"></div>
						<input type="button" class="button sm fixed-width" value="Go to Dashboard" />
						<div class="spacer"></div>
					</div>
					<div class="spacer"></div>
					<div class="function_box center">
						<div class="spacer"></div>
						<h4>Aktuary Advice</h4>
						<blockquote class="quote align-left">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit mi id varius eleifend. Ut id lectus a nulla euismod consectetur. Donec pharetra ac magna non luctu.</p>
						</blockquote>
						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="spacer ten"></div>
<?php
	require 'includes/footer.inc';
?>