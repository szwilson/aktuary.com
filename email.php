<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--[if !mso]><!-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!--<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link type="text/css" rel="stylesheet" href="css/email.css" >
	<title></title>
</head>
<body>
	<center class="wrapper">
		<div class="webkit">
			<table class="outer" align="center">
			    <tr class="header">
			        <td>
						<img src="img/email_header.png" width="233" alt="Aktuary.com" />
			        </td>
			    </tr>
			    
			    <tr>
				    <td class="inner-wrapper">
					    <table class="inner" align="center">
					    	<tr>
						    	<td>
							    	<h1>Retirement Income Report</h1>
						    	</td>
					    	</tr>
					    	<tr>
						    	<td>
									<img src="img/email_inline.png" width="520" alt="Aktuary.com" />
						    	</td>
					    	</tr>
					    	<tr class="body">
						    	<td>
							    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis pharetra eros in faucibus. Aliquam scelerisque ligula non eros rutrum, id lacinia est lobortis. Suspendisse interdum justo ac justo porttitor dapibus. Cras sit amet sagittis turpis, in condimentum lacus. Sed ac mi massa.</p>
							    	<p><strong>Download your retirement income report now.</strong></p>
						    	</td>
					    	</tr>
					    	<tr>
						    	<td>
							    	<a href="#" class="button">Download Your Report</a>
						    	</td>
					    	</tr>
					    </table>
					    <table class="inner-wrapper-shadow" border="0" cellpadding="0" cellspacing="0">
						    <tr><td><img src="img/email_content_shadow.png" /></td></tr>
					    </table>
				    </td>
			    </tr>
			</table>
			<table class="footer" align="center">
				<tr>
					<td>
						<p><em>Copyright &copy; 2015 Aktuary.com. All rights reserved.</em></p>
						<p>Want to change how you receive these emails?<br />
							You can <a href="#">update your preferences</a> or <a href="#">unsubscribe from this list</a>.
						</p>
						
					</td>
				</tr>
			</table>
		</div>
	</center>
</body>
</html>